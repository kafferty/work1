//формировать расписание звонков в школе. Время начала 9:00. Количество уроков, длительность урока, длительность малого и большого перерыва, числова уроков до большого перерыва задаются пользователем
#include <iostream>
#include <cstdio>
#include <iomanip>

using namespace std;

int main(void)
{
const int minInHour = 60;
const int startTime = 9 * minInHour;
int Number, DurLess, DurSmBr, DurLongBr, NumbLess;
int currTime = startTime;
cout<<"Please enter the number of lesson, lesson duration, duration of the small and long break, the number of lessons before the long break"<<endl;
cin >>Number;
cin >>DurLess;
cin>>DurSmBr;
cin>>DurLongBr;
cin>>NumbLess;
for (int i = 1; i <= Number; i++)
{
	if (i == NumbLess) {
	cout<<"Lesson number"<<" "<<i<<endl;
	cout<<"Start:"<<" "<<setfill('0')<<setw(2)<<currTime / minInHour<<":";
	cout<<setfill('0')<<setw(2)<<currTime%minInHour<<endl;
	currTime += DurLess;
	cout<<"End:"<<" "<<setfill('0')<<setw(2)<<currTime / minInHour<<":";
	cout<<setfill('0')<<setw(2)<<currTime%minInHour<<endl;
	currTime = currTime + DurLongBr;
	i++;
	}
cout<<"Lesson number"<<" "<<i<<endl;
cout<<"Start:"<<" ";
cout<<setfill('0')<<setw(2)<<currTime / minInHour<<":";
cout<<setfill('0')<<setw(2)<<currTime%minInHour<<endl;
currTime += DurLess;
cout<<"End:"<<" "<<setfill('0')<<setw(2)<<currTime / minInHour<<":";
cout<<setfill('0')<<setw(2)<<currTime%minInHour<<endl;
currTime += DurSmBr;

}
return 0;
}



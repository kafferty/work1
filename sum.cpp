//Задано целое число от 0 до 999. Определить сумму его цифр.
#include <iostream>
using namespace std;

int main() {
	int a,i;
	i=0;
	cout << "Enter your number from 0 to 999" << endl;
	cin >> a;
	while (a>0)
	{
		i=i+a%10;
		a=a/10;
	}
	cout << i << endl;
	return 0;
}

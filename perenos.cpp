//Расстановка переносов в словах
//Городничева Лидия. 13501/3
#include <iostream>
#include <fstream>

using namespace std;

//Проверка гласной буквы
bool isGlas(char letter) {

    char l = tolower(letter);

    char a[] = "аоэиуыеёюя";
    for (int i = 0; i < strlen(a); i++) {
        if (a[i] == l)
            return true;
    }
    return false;
}

//Проверка согласной буквы
bool isSoglas(char letter) {

    char l = tolower(letter);
    char a[ ] = "бвгдйжзклмнпрстфхцчшщ";
    for (int i = 0; i < strlen(a); i++) {
        if (a[i] == l)
            return true;
    }
    return false;
}

//Проверка на возможность переноса
bool canHyphenAtPosition(string word, int i) {

    //Начало слова
    if (i == 0) return false;

    //Конец слова
    if (i == (word.length() - 1)) return false;

    //Специальные символы
    if (!isSoglas(word[i]) && !isGlas(word[i])) return false;

    string hypenExceptions = "эыъ";
    //Запрещенные для переноса буквы
    if (hypenExceptions.find(word[i+1]) > 0) return false;

    //Правило гласных и согласных
    if (isSoglas(word[i]) && isGlas(word[i + 1])) return false;

    return true;
}

//Обработка одного слова и запись в выходной поток
void processWord(const string& word, ofstream &out) {

    int length = word.length();

    //Перебор по буквам
    for (int i = 0; i <= length; i++) {
        if (canHyphenAtPosition(word,i)) {
            //Перенос разрешен  - пишем букву и знак переноса
            out << word[i];
            out << "-";
        } else {
            //Перенос запрещен - пишем только букву
            out << word[i];
        }
    }

    out << " "; //Пишем пробел в конце слова
}

//Основной блок - читаем входной файл по словам
int main() {
    ifstream in("in.txt");
    ofstream out("out.txt");

    if (in.is_open()) {
        while (in) {
            string word;
            in >> word;
            processWord(word, out);
        }
    }

    return 0;
}

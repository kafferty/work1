/*
 Gorodnicheva Lidia, 13501/3
 Class Rational
*/

#ifndef RATIONAL_H
#define	RATIONAL_H
#include <iostream>
using namespace std;
class rational

{  int num;
int den;
friend istream &operator>>(istream&, rational&);
friend ostream &operator<<(ostream&, rational&);
int nod(int, int);
int abs(int);
public: rational(int=0, int=1);
void norm();
rational operator*(const rational&) const;
rational operator+(const rational&) const;
rational operator-(const rational&) const;
rational operator/(const rational&) const;
};
#endif
};
